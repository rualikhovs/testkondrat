/*
 * Работа с матрицами
 * Взято с
 * http://mathhelpplanet.com/static.php?p=javascript-operatsii-nad-matritsami
 */

/*
 * Транспонирование матрицы
 */
function transMatrix(A) {
  if(A == null){
    console.error("Часть параметров не определена");
    return null;
  }
  let m = A.length, n = A[0].length, AT = [];
  for (var i = 0; i < n; i++){
    AT[i] = [];
    for (var j = 0; j < m; j++) AT[i][j] = A[j][i];
  }
  return AT;
}

/*
 * Сложение матриц
 */
function sumMatrix(A,B) {
  if(A == null || B == null){
    console.error("Часть параметров не определена");
    return null;
  }
  if(A.length != B.length || A[0].length != B[0].length){
    console.error("Размеры матриц не совпадают");
    return null;
  }
  let m = A.length, n = A[0].length, C = [];
  for (var i = 0; i < m; i++) {
    C[i] = [];
    for (var j = 0; j < n; j++) C[i][j] = A[i][j]+B[i][j];
  }
  return C;
}

/*
 * Вычитание матриц
 */
function subMatrix(A,B) {
  if(A == null || B == null){
    console.error("Часть параметров не определена");
    return null;
  }
  if(A.length != B.length || A[0].length != B[0].length){
    console.error("Размеры матриц не совпадают");
    return null;
  }
  let m = A.length, n = A[0].length, C = [];
  for (var i = 0;i < m;i++) {
    C[i] = [];
    for (var j = 0;j < n;j++) C[i][j] = A[i][j]-B[i][j];
  }
  return C;
}

/*
 * Умножение матрицы на число
 */
function multMatrixNumber(A, a){//(a,A){
  let m = A.length, n = A[0].length, B = [];
  for (var i = 0; i < m; i++){
    B[i] = [];
    for (var j = 0; j < n; j++) B[i][j] = a*A[i][j];
  }
  return B;
}

/*
 * Перемножение матриц
 */
function multiplyMatrix(A,B) {
  if(A == null || B == null){
    console.error("Часть параметров не определена");
    return null;
  }
  let rowsA = A.length, colsA = A[0].length,
      rowsB = B.length, colsB = B[0].length;
  let res = new Array();
  if (colsA != rowsB) return null;
  for (var i = 0; i < rowsA; i++) res[i] = [];
  for (var k = 0; k < colsB; k++)
    { for (var i = 0; i < rowsA; i++)
      { var t = 0;
        for (var j = 0; j < rowsB; j++) t += A[i][j]*B[j][k];
      res[i][k] = t;
    }
  }
  return res;
}

/*
 * Возведение матрицы в степень
 */
function matrixPow(A, n){//(n, A) { 
  if(A == null){
    console.error("Часть параметров не определена");
    return null;
  }
  if (n == 1)
    return A;
  else
    return multiplyMatrix( A, matrixPow(A, n-1) );
}

/*
 * вычисление определителя матрицы
 */
function determinant(A) {
  let N = A.length, B = [], denom = 1, exchanges = 0;
  for (var i = 0; i < N; ++i){
    B[i] = [];
    for (var j = 0; j < N; ++j) B[i][j] = A[i][j];
  }
  for (var i = 0; i < N-1; ++i) {
    let maxN = i, maxValue = Math.abs(B[i][i]);
    for (var j = i+1; j < N; ++j) {
      let value = Math.abs(B[j][i]);
      if (value > maxValue){ maxN = j; maxValue = value; }
    }
    if (maxN > i) {
      let temp = B[i]; B[i] = B[maxN]; B[maxN] = temp;
      ++exchanges;
    } else if(maxValue == 0) {
      return maxValue;
    }
    let value1 = B[i][i];
    for (var j = i+1; j < N; ++j) {
      let value2 = B[j][i];
      B[j][i] = 0;
      for (var k = i+1; k < N; ++k) B[j][k] = (B[j][k]*value1-B[i][k]*value2)/denom;
    }
    denom = value1;
  }
  if (exchanges%2) return -B[N-1][N-1];
  else return B[N-1][N-1];
}

/*
 * Вычисление союзной матрицы для квадратной матрицы
 */
function adjugateMatrix(A) {
  if(A == null){
    console.error("Часть параметров не определена");
    return null;
  }
  if(A.length != A[0].length){
    console.err("Матрица должна быть квадратной");
    return null;
  }
  let N = A.length, adjA = [];
  for (var i = 0;i < N;i++) {
    adjA[i] = [];
    for (var j = 0;j < N;j++) {
      let B = [], sign = ((i+j)%2==0) ? 1 : -1;
      for (var m = 0;m < j;m++) {
        B[m] = [];
        for (var n = 0;n < i;n++)   B[m][n] = A[m][n];
        for (var n = i+1;n < N;n++) B[m][n-1] = A[m][n];
      }
      for (var m = j+1;m < N;m++) {
        B[m-1] = [];
        for (var n = 0;n < i;n++)   B[m-1][n] = A[m][n];
        for (var n = i+1;n < N;n++) B[m-1][n-1] = A[m][n];
      }
      adjA[i][j] = sign*determinant(B);  // Функцию Determinant см. выше
    }
  }
  return adjA;
}

/*
 * Инвертирование квадратной матрицы
 */
function inverseMatrix(A) {   
  if(A == null){
    console.error("Часть параметров не определена");
    return null;
  }
  let det = determinant(A);
  if (det == 0) return null;
  let N = A.length; 
  A = adjugateMatrix(A);
  for (var i = 0;i < N;i++) {
    for (var j = 0;j < N;j++) A[i][j] /= det;
  }
  return A;
}