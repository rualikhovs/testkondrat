function printTableCell(matrixType, matrixName, matrixIndex, matrix) {
    document.write("<td>");
    document.write(matrixName + "<sub>" + matrixIndex + "</sub>" + "<br>");
    switch(matrixType) {
    case "matrix":
    	printMatrix(matrix);
    	break;
    case "vector":
    	printVector(matrix);
    	break;
    }    
    document.write("</td>");
}

//точка входа
window.onload = function() {
        calmanFilter();
        calmanSystemAnalisis();
        regulatiorSynthesis();        
        document.write("<h3>Оптимальное оценивание состояния в несинхронных системах.</h3>");
        document.write("<p>Входные матрицы.</p>");
        document.write('<table border = "1">');
        document.write("<tr>");
        printTableCell("matrix", "A", "", A);
        printTableCell("matrix", "B", "", B);
        printTableCell("matrix", "C", "", C);
        printTableCell("vector", "U", "", U);
        printTableCell("matrix", "H", "", H);
        printTableCell("matrix", "R", "", R);
        printTableCell("matrix", "Q", "", Q);
        document.write("</tr>");
        document.write("</table>");
        
        document.write("<p>Результат.</p>");
        document.write('<table border = "1">');
        document.write("<tr>");
        printTableCell("matrix", "F", "k+1", F);
        printTableCell("vector", "x", "k+1", nextX);
        printTableCell("matrix", "P", "k+1", nextP);
        document.write("</tr>");
        document.write("</table>");
        
        test3_2();
        
        document.write("<h3>Ковариационный анализ замкнутых несинхронных систем.</h3>");
        document.write("<p>Входные матрицы.</p>");
        document.write('<table border = "1">');
        document.write("<tr>");
        printTableCell("matrix", "A", "", A);
        printTableCell("matrix", "B", "", B);
        printTableCell("matrix", "G", "", G);
        printTableCell("matrix", "F", "", F);
        printTableCell("matrix", "H", "", H);
        printTableCell("matrix", "R", "", R);
        printTableCell("matrix", "P", "", nextP);
        printTableCell("matrix", "Phi", "", Phi);
        printTableCell("matrix", "Psi", "", Psi);
        document.write("</tr>");
        document.write("</table>");
        
        document.write("<p>Результат.</p>");
        document.write('<table border = "1">');
        document.write("<tr>");
        printTableCell("matrix", "P", "xk", Pxk);
        printTableCell("matrix", "P", "uk", Puk);
        printTableCell("matrix", "J", "s", J);
        document.write("</tr>");
        document.write("</table>");
        //test3_2();
}
