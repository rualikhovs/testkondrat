/*
 * Функции с конца страницы 85
 * A B C Q R G U - константные матрицы
 */

/*
 * 1-я формула из 3.30
 */
function nextTilOmega (Perr, Pek, Nk) {
  let transpN = transMatrix(Nk);
  let matr1 = multiplyMatrix(multiplyMatrix(Nk, Pek),transpN);
  let res = subMatrix(Perr, matr1);
  return res;
}

/*
 * 2-я формула из 3.30
 */
function omegaKplusOneDivK (A, N, Pek, C, Q) {
  let transpA = transMatrix(A);
  let transpC = transMatrix(C);
  let transpN = transMatrix(N);
  let matr1 = multiplyMatrix(A, N);
  let matr2 = multiplyMatrix(matr1, Pek);
  let matr3 = multiplyMatrix(matr2, transpN);
  let matr4 = multiplyMatrix(matr3, transpA);
  
  let matr5 = multiplyMatrix(C, Q);
  let matr6 = multiplyMatrix(matr5, transpC);
  
  let matr7 = sumMatrix(matr4, matr6);
  return matr7;
}

/*
 * 3-я формула из 3.30
 */
function nextOmega(Perr, Pek, A, N, C, Q) {
  let matr1 = nextTilOmega(Perr, Pek, N);
  let matr2 = omegaKplusOneDivK(A, N, Pek, C, Q);
  return sumMatrix(matr1, matr2);
}

/*
 * 4-я формула из 3.30
 */
function nextK(omega11, omega21, R, S) {
  if(0 == S){
    let matr1 = matrixPow(omega11, 1);
    let matr2 = multiplyMatrix(omega21, matr1);
    let matr3 = multMatrixNumber(matr2, 0.5);
    return matr3;
  } else if (1 == S) {
    let matr1 = matrixPow(sumMatrix(omega11, R), 1);
    let matr2 = multiplyMatrix(omega21, matr1);
    return matr2;
  } else {
    console.log("Неверное значение S");
  }
}

/*
 * 5-я формула из 3.30
 */
function nextPEK(T, omega) {
  let transpT = transMatrix(T);
  let matr1 = multiplyMatrix(T, omega);
  let matr2 = multiplyMatrix(matr1, transpT);
  return matr2;
}

/*
 * 6-я формула из 3.30
 */
function nextPErr(A, C, H, L, N, Pek, Perr, Q, R, T, omega, S) {
  let transpN = transMatrix(N);
  let transpL = transMatrix(l);
  if(0 == S){
    let transpA = transMatrix(A);
    let transpC = transMatrix(C);
    let transpH = transMatrix(H);
    let transpT = transMatrix(T);
    let matr1 = multiplyMatrix(multiplyMatrix(N, Pek),transpN);//<<
    
    let matr2 = multiplyMatrix(N,T);
    let matr3 = multiplyMatrix(matr2,omega);
    let matr4 = multiplyMatrix(matr3,transpH);
    let matr5 = multiplyMatrix(matr4,transpL);//<<
    
    let matr6 = multiplyMatrix(L,H);
    let matr7 = multiplyMatrix(matr6,omega);
    let matr8 = multiplyMatrix(matr7,transpT);
    let matr9 = multiplyMatrix(matr8,transpN);//<<
    
    let matr10 = multiplyMatrix(A,Perr);
    let matr11 = multiplyMatrix(matr10,transpA);
    let matr12 = multiplyMatrix(C,Q);
    let matr13 = multiplyMatrix(matr12,transpC);
    let matr14 = sumMatrix(matr11,matr13);//<< сумма в скобках
    
    let matr15 = multiplyMatrix(L,H);
    let matr16 = multiplyMatrix(matr15,matr14);
    let matr17 = multiplyMatrix(matr16,transpH);
    let matr18 = multiplyMatrix(matr17,transpL);
    
    return sumMatrix(matr1, sumMatrix(matr5, sumMatrix(matr9, matr18)));
  } else if (1 == S) {
    let matr1 = multiplyMatrix(N, Pek);
    let matr2 = multiplyMatrix(Matr1, transpN);
    
    let matr3 = multiplyMatrix(L, R);
    let matr4 = multiplyMatrix(matr3, transpL);
    return sumMatrix(matr2, matr4);
  } else {
    console.log("Неверное значение S");
  }
}

/*
 * 7-я формула из 3.30
 */
function nextPXeps(A, C, H, L, Nk, Nkp1, Pek, Pxek, T, Q, S) {
  if(0 == S){
    let transpA = transMatrix(A);
    let transpC = transMatrix(C);
    let transpN = transMatrix(Nk);
    let matr1 = multiplyMatrix(Nk,Pek);
    
    let matr2 = multiplyMatrix(A,Pxek);
    let matr3 = multiplyMatrix(matr2,transpN);
    let matr4 = multiplyMatrix(matr3,transpA);
    let matr5 = multiplyMatrix(C,Q);
    let matr6 = multiplyMatrix(matr5,transpC);
    let matr7 = sumMatrix(matr4,matr6);//<<сумма с скобках
    
    let matr8 = multiplyMatrix(L,H);
    let matr9 = multiplyMatrix(matr8,matr7);
    let matr10 = multiplyMatrix(matr9,transMatrix(T));
    
    return sumMatrix(matr1,matr10);
  } else if (1 == S) {
    return multiplyMatrix(Nk,Pek);
  } else {
    console.log("Неверное значение S");
  }
}

function test3_2(){
  var zeroes = createZeroMatrix(3,3);
  var A = createMatrix(3,3);
  var C = createMatrix(3,3);
  var H = createMatrix(3,3);
  var L = createMatrix(3,3);
  var Q = createMatrix(3,3);
  var R = createMatrix(3,3);
  var T = createMatrix(3,3);
  var Pek = createMatrix(3,3);
  var Perr = createMatrix(3,3);//матрица ошибки корреляции
  var Nk = createMatrix(3,3);

  let matr1 = nextTilOmega (Perr, Pek, Nk);
  let matr2 = omegaKplusOneDivK(A, Nk, Pek, C, Q);
  let matr3 = nextOmega(Perr, Pek, A, Nk, C, Q);//omega
  let matr4 = nextK(matr3, zeroes, R, 0);
  let matr5 = nextK(matr3, zeroes, R, 1);
  let matr6 = nextPEK(T, matr3);
  let matr7 = nextPXeps(A, C, H, L, Nk, Nk, matr6, Perr, T, Q, 0);
  let matr8 = nextPXeps(A, C, H, L, Nk, Nk, matr6, Perr, T, Q, 1);
  
  document.write("<h3>Построение наблюдателя минимальной размерности.</h3>");
  document.write("<p>Входные матрицы.</p>");
  document.write('<table border = "1">');
  document.write("<tr>");
  printTableCell("matrix", "A", "", A);
  printTableCell("matrix", "C", "", C);
  printTableCell("matrix", "H", "", H);
  printTableCell("matrix", "L", "", L);
  printTableCell("matrix", "Q", "", Q);
  printTableCell("matrix", "R", "", R);
  printTableCell("matrix", "T", "", T);
  document.write("</tr>");
  document.write("</table>");
  
  document.write("<p>Результат.</p>");
  document.write('<table border = "1">');
  document.write("<tr>");
  //printTableCell("vector", "P", "xek+1", matr7);
  printTableCell("matrix", "P", "xek+1", matr8);
  document.write("</tr>");
  document.write("</table>");
  
  console.log("Готово");
}