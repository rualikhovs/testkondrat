/**
 * 
 */

var A = createMatrix(3,3); // Матрица связанная с состоянием
var B = createMatrix(3,3);  // Матрица связанная с контролем
var C = createMatrix(3,3);  // Матрица связанная с шумом
var H = createMatrix(3,3);  // Матрица измерителя
var F = createZeroMatrix(3,3); // Матрица коэфициента фильтрации фильтра Калмана
var Q = createMatrix(3,3); // Ковариационная матрица шума процесса
var R = createMatrix(3,3); // Ковариационная матрица шума измерения
var U = createMatrix(1,3); // Вектор управления
var G = createMatrix(3,3); // Матрица регулятора
var J = createMatrix(3,3); // Матрица качества системы
var Phi = createMatrix(3,3); // Весовая матрица квадратичного функционала
var Psi = createMatrix(3,3); // Весовая матрица квадратичного функционала
var Pxk = createMatrix(3,3); // Ковариационная матрица состояния
var Puk = createMatrix(3,3); // Ковариационная матрица управления
var S = 1; // Наблюдение из стратегии наблюдения
var currX = createMatrix(1,3);
var currP = createMatrix(3,3);
var nextY = createMatrix(1,3);
var newTheta = createMatrix(3,3);
var Pk = createMatrix(3,3); //Корректирующая матрица 

var I = [
	[1, 0, 0],
	[0, 1, 0],
	[0, 0, 1],
	];
var nextP, nextX; //Ошибка оценки, оценка состояния
var k = 1;
var N = 1;

function getRandomArbitrary(min, max) {
        return Math.random() * (max - min) + min;
}

function createMatrix(rows,columns) { //Заполнитель матриц
        var arr = new Array();
        for(var i=0; i<columns; i++){
                arr[i] = new Array();
                for(var j=0; j<rows; j++){
                        arr[i][j] = getRandomArbitrary(0.0, 1.0);
                }
        }
        return arr;
}

function createZeroMatrix(rows,columns) { //Заполнитель матриц
        var arr = new Array();
        for(var i=0; i<columns; i++){
                arr[i] = new Array();
                for(var j=0; j<rows; j++){
                        arr[i][j] = 0.0;
                }
        }
        return arr;
}

function printMatrix(matrix) {
        for(let j = 0;j < matrix.length;j++) {
                for(let n = 0;n < matrix.length;n++) {
                        if (n === matrix.length - 1)
                                document.write(matrix[j][n].toFixed(2));
                        else
                                document.write(matrix[j][n].toFixed(2) + ", ");
                }
                document.write("<br>");
        }       
}

function printVector(vector) {
        for(let i = 0;i < vector.length;i++) {
                if(i === vector.length - 1)
                        document.write(Math.round(vector[i] * 100) / 100);
                else
                        document.write(Math.round(vector[i] * 100) / 100 + ", ");              
        }
        document.write("<br>");
}

//Фильтер Калмена. Последовательность формул (3.4) -> (3.6) -> (3.5) -> (3.3) -> (3.7)
function calmanFilter() {
        var transA = transMatrix(A);
        var transC = transMatrix(C);
        var transH = transMatrix(H);
        // Предсказание
        var x = sumMatrix(multiplyMatrix(A, currX), multiplyMatrix(B, U));
        var intermedValue1 = multiplyMatrix(A, multiplyMatrix(currP, transA));
        var intermedValue2 = multiplyMatrix(C, multiplyMatrix(Q, transC));
        var P = sumMatrix(intermedValue1, intermedValue2);
        // Корректировка
        if(S == 0) {
                F = createZeroMatrix(3,3);
                var y = subMatrix(nextY, multiplyMatrix(H, x));
                nextX = sumMatrix(x, multiplyMatrix(F, y));
                var intermedValue4 = subMatrix(I, multiplyMatrix(F, H));
                nextP = multiplyMatrix(intermedValue4, P);
        } else {
                var intermedValue3 = multiplyMatrix(H, multiplyMatrix(P, transH));
                var inverseValue = inverseMatrix(sumMatrix(intermedValue3, R));
                F = multiplyMatrix(P, multiplyMatrix(transH, inverseValue));
                var y = subMatrix(nextY, multiplyMatrix(H, x));
                nextX = sumMatrix(x, multiplyMatrix(F, y));
                var intermedValue4 = subMatrix(I, multiplyMatrix(F, H));
                nextP = multiplyMatrix(intermedValue4, P);
        }       
}

//Анализ системы фильтром Калмена. Последовательность формул (3.33) -> (3.34) -> (3.35) -> (3.32)
function calmanSystemAnalisis() {
        var transH = transMatrix(H);
        var transF = transMatrix(F);
        var transG = transMatrix(G);
        var product1 = createMatrix(3,3);
        for(let i = 0;i < k;i++) {                
                product1 = multiplyMatrix(product1, sumMatrix(A, multiplyMatrix(B, G)));
        }
        for(let i = 0;i < k;i++) {
                var transMx = transMatrix(sumMatrix(A, multiplyMatrix(B, G)));
                var product2 = createMatrix(3,3);
                priduct2 = multiplyMatrix(product2, transMx);
        }
        var P = multiplyMatrix(product1, multiplyMatrix(currP, product2));
        var intermedValue1 = sumMatrix(multiplyMatrix(H, multiplyMatrix(P, transH)), R);
        var newP = sumMatrix(P, multiplyMatrix(F, multiplyMatrix(intermedValue1, transF)));
        Pxk = sumMatrix(newP, nextP);
        Puk = multiplyMatrix(G, multiplyMatrix(newP, transG));
        for(let i = 0;i < N;i++) {
                product1 = multiplyMatrix(Phi, Pxk);
                product2 = multiplyMatrix(Psi, Puk);
                J = sumMatrix(J, sumMatrix(product1, product2));
        }
}

//Синтез регулятора. Последовательность формул (3.46)
function regulatiorSynthesis() {
	var transB = transMatrix(B);
	var intermedValue1 = sumMatrix(multiplyMatrix(multiplyMatrix(transB, newTheta), B), Psi);
	intermedValue1 = inverseMatrix(intermedValue1);
	intermedValue1 = multMatrixNumber(intermedValue1, -1);
	var intermedValue2 = multiplyMatrix(multiplyMatrix(transB, newTheta), multiplyMatrix(A, Pk));
	G = multiplyMatrix(intermedValue1, intermedValue2);	
}